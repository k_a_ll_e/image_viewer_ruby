require 'gosu'

IMAGE_SPACING = 100.0

class ImageContainer
	attr_reader :x
	attr_writer :playing

	def move(vel_x)
		@x += vel_x if @playing
	end

	def draw
		@image_file.draw_rot(@x, @y, 1, 0.0,
												 0.0, 0.0, @scale_factor,
												 @scale_factor) 

		Gosu::draw_rect(
			@x - IMAGE_SPACING / 2.0, 0.0,
			scaled_width + IMAGE_SPACING, @screen_height,
			@color, 0.0)
	end

	def outside_right_border?
		return false if @did_send_outside_right_border
		@did_send_outside_right_border = @x - IMAGE_SPACING / 2 >= @screen_width
	end

	def first_time_visible?
		return false if @did_send_visible
		@did_send_visible = @x + scaled_width > 0
	end

	def shift_left_of_previous_image(x_prev)
		@x = x_prev - (IMAGE_SPACING + scaled_width)
	end

	private

	attr_reader :screen_height, :screen_width
	attr_reader :y, :color, :image_file
	attr_reader :scale_factor, :image_file
	attr_reader :did_send_visible, :did_send_outside_right_border

	def initialize(path, width, height)
		@screen_width, @screen_height = width, height

		@color = determine_color(path)

		@image_file = Gosu::Image.new(path)
		@did_send_visible = @did_send_outside_right_border = false

		image_height = @image_file.height
		desired_height = height - IMAGE_SPACING
		@scale_factor = desired_height / image_height

		@x, @y, @playing = 0.0, IMAGE_SPACING / 2.0, true
	end

	def determine_color(path)
		return Gosu::Color::BLACK if path.nil?

		if path.include?('red@')
			Gosu::Color.argb(0xff_aa0000)
		elsif path.include?('blue@')
			Gosu::Color.argb(0xff_0080ff)
		elsif path.include?('yellow@')
			Gosu::Color.argb(0xff_ffee5c)
		elsif path.include?('green@')
			Gosu::Color.argb(0xff_00994c)
		elsif path.include?('orange@')
			Gosu::Color.argb(0xff_ff8417)
		elsif path.include?('black@')
			Gosu::Color::BLACK
		elsif path.include?('silver@')
			Gosu::Color.argb(0xff_a0a0a0)
		elsif path.include?('brown@')
			Gosu::Color.argb(0xff_d1c2a0)
		elsif path.include?('white@')
			Gosu::Color::WHITE
		else
			Gosu::Color::BLACK
		end
	end

	def scaled_width
		image_file.width * scale_factor
	end

end
