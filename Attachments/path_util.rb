require 'set'

class FileGetter  

  def next_file  
		showable_images = get_all_image_files - @known_images

		unless showable_images.any?
			return '' unless @known_images.any?
			if @known_images.count == @shown_images.count then
				@shown_images.clear
				File.delete('shown.txt') if File.exist?('shown.txt')
			end
			showable_images = @known_images - @shown_images
		end

		selected_image = showable_images.to_a.sample
		@known_images.add(selected_image)
		@shown_images.add(selected_image)
		['known.txt', 'shown.txt'].map{|f| append_to_file(selected_image, f)}

		return selected_image
  end  

	private

  def initialize
		@known_images = File.exist?('known.txt') ? deserialize('known.txt') : Set.new()
		@shown_images = File.exist?('shown.txt') ? deserialize('shown.txt') : Set.new()
		['known.txt', 'shown.txt'].map{|f| File.delete(f) if File.exist?(f)}
	end

	def get_all_image_files
		image_dirs = Dir.glob('*')
			.select{|m| m.include? '@kfsvensson.se'}

		image_files = image_dirs
			.map{|dir| Dir.glob(File.join(dir, 'resized_*'))}
			.flatten(1)
			.to_set

		unless image_files
			image_files = image_dirs
				.map{|dir| Dir.glob(File.join(dir, '*'))}
				.flatten(1)
				.to_set
		end

		return image_files
	end

	def append_to_file(line, name)
		File.open(name, 'a') do |f|
			f.puts line
		end
	end

	def deserialize(name)
		lines = Set.new()
		File.open(name, 'r') do |f|
			f.each_line do |line|
				lines.add(line.strip)
			end
		end

		return lines
	end

end  
