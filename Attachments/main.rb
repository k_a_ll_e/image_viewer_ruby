require 'gosu'
require_relative 'path_util'
require_relative 'image_container'

MAX_SPEED, SPEED_INCREASE = 10.0, 0.2

class ImageViewer < Gosu::Window

	attr_reader :vel_x, :playing, :mirrored

	def initialize
		@screen_width, @screen_height = 1680, 1050
		super @screen_width, @screen_height, true
		self.caption = "ImageViewer"
		@file_getter, @images = FileGetter.new(), Array.new
		@vel_x, @playing = 0.5, true
	end

	def button_down(id)
		case id
		when Gosu::KbEscape
			close
		when Gosu::KB_Q
			close
		end
	end

	def button_up(id)
		case id
		when Gosu::KB_UP
			@vel_x += SPEED_INCREASE
			@vel_x = [@vel_x, MAX_SPEED].min
		when Gosu::KB_DOWN
			@vel_x -= SPEED_INCREASE
			@vel_x = [@vel_x, 0.0].max
		when Gosu::KB_SPACE
			@playing = !@playing
		when Gosu::KB_M
			@mirrored = !@mirrored
		end
	end

	def update
		try_add_image
		@images.shift if @images.first.outside_right_border?
		@images.each{|i| i.playing = @playing}.each{|i| i.move(@vel_x)}
	end

	def draw
		a = @mirrored ? -1 : 1
		x = @mirrored ? @screen_width : 0
		Gosu::transform(a, 0, 0, 0,
										0, 1, 0, 0,
										0, 0, 1, 0,
										x, 0, 0, 1) {@images.each{|i| i.draw}}
	end

	def try_add_image
		def add_image
			new_img = ImageContainer.new(@file_getter.next_file, @screen_width, @screen_height)
			x_shift = @images.any? ? @images.last.x : 0
			new_img.shift_left_of_previous_image(x_shift)
			@images.push(new_img)
		end

		add_image if !@images.any? or @images.any?{|i| i.first_time_visible?}
	end

end


ImageViewer.new.show
